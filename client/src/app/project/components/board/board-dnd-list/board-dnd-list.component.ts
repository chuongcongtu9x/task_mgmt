import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {IssueStatus, IssueStatusDisplay, JIssue} from '@trungk18/interface/issue';
import {FilterState} from '@trungk18/project/state/filter/filter.store';
import {ProjectService} from '@trungk18/project/state/project/project.service';
import {Observable, combineLatest} from 'rxjs';
import {untilDestroyed, UntilDestroy} from '@ngneat/until-destroy';
import * as dateFns from 'date-fns';
import {IssueUtil} from '@trungk18/project/utils/issue';
import {BoardService} from '@trungk18/services/board.service';
import {NzModalService} from 'ng-zorro-antd';
import {AddIssueModalComponent} from '../../add-issue-modal/add-issue-modal.component';
import { IssueDeleteModalComponent } from '../../issues/issue-delete-modal/issue-delete-modal.component';
import { IssueModalComponent } from '../../issues/issue-modal/issue-modal.component';

@Component({
  selector: '[board-dnd-list]',
  templateUrl: './board-dnd-list.component.html',
  styleUrls: ['./board-dnd-list.component.scss'],
})
@UntilDestroy()
export class BoardDndListComponent implements OnInit {
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @Output() deletteTask: EventEmitter<any> = new EventEmitter<any>();

  @Input() status: any;
  @Input() currentUserId: string;
  @Input() issues$: Observable<any[]>;
  issues: any[] = [];
  @Input() users: any[];
  

  get issuesCount(): number {
    return this.issues.length;
  }

  constructor(
    private _projectService: ProjectService,
    private boardService: BoardService,
    private _modalService: NzModalService
  ) {
  }

  ngOnInit(): void {
  }

  drop(event: CdkDragDrop<string[]>) {
    const preTaskItem = {...event.item.data};
    const newTaskItem = [...event.container.data] as any[];

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      // update position
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      // update issue

      preTaskItem.taskId = newTaskItem[0].taskId;

      this.boardService.updateTaskIdForItem(preTaskItem).subscribe(res => {
      });
    }
  }

  private updateListPosition(newList: JIssue[]) {
    newList.forEach((issue, idx) => {
      const newIssueWithNewPosition = {...issue, listPosition: idx + 1};
      this._projectService.updateIssue(newIssueWithNewPosition);
    });
  }

  filterIssues(issues: JIssue[], filter: FilterState): JIssue[] {
    const {onlyMyIssue, ignoreResolved, searchTerm, userIds} = filter;
    return issues.filter((issue) => {
      const isMatchTerm = searchTerm
        ? IssueUtil.searchString(issue.title, searchTerm)
        : true;

      const isIncludeUsers = userIds.length
        ? issue.userIds.some((userId) => userIds.includes(userId))
        : true;

      const isMyIssue = onlyMyIssue
        ? this.currentUserId && issue.userIds.includes(this.currentUserId)
        : true;

      const isIgnoreResolved = ignoreResolved ? issue.status !== IssueStatus.DONE : true;

      return isMatchTerm && isIncludeUsers && isMyIssue && isIgnoreResolved;
    });
  }

  isDateWithinThreeDaysFromNow(date: string) {
    const now = new Date();
    const inputDate = new Date(date);
    return dateFns.isAfter(inputDate, dateFns.subDays(now, 3));
  }

  openCreateIssueModal(status: any) {
    const dialog = this._modalService.create({
      nzContent: AddIssueModalComponent,
      nzClosable: false,
      nzFooter: null,
      nzWidth: 700,
      nzComponentParams: {
        task: status
      }
    });

    dialog.afterClose.subscribe(res => {
      this.modalSave.emit(res);
    });
  }

  openDeleteIssueModal(task) {
    this.deletteTask.emit(task);
  }
}
