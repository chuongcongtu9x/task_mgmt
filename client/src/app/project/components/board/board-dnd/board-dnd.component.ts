import { Component, Input, OnInit } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { IssueStatus } from '@trungk18/interface/issue';
import { ProjectQuery } from '@trungk18/project/state/project/project.query';
import { AuthQuery } from '@trungk18/project/auth/auth.query';
import { BoardService } from '@trungk18/services/board.service';
import { FormControl } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd';
import { AddTaskModalComponent } from '../../add-task-modal/add-task-modal.component';
import { Router } from '@angular/router';
import { IssueDeleteModalComponent } from '../../issues/issue-delete-modal/issue-delete-modal.component';
@UntilDestroy()
@Component({
  selector: 'board-dnd',
  templateUrl: './board-dnd.component.html',
  styleUrls: ['./board-dnd.component.scss']
})
export class BoardDndComponent implements OnInit {
  issueStatuses: any[] = [];
  searchControl: FormControl = new FormControl('');
  userIds: string[];
  @Input() board: any;
  users: any[];

  constructor(
    public projectQuery: ProjectQuery,
    public authQuery: AuthQuery,
    private readonly boardService: BoardService,
    private _modalService: NzModalService,
    private readonly router: Router,
  ) {

    setTimeout(() => {
      this.boardService.getTask().subscribe(res => {
        if (res) {
          res.map(e => {
            if (e.boardId === this.board._id) {
              this.issueStatuses.push(e);
            }
          });
        }
      });
    }, 100);
  }

  ngOnInit(): void {
    this.boardService.getUsers().subscribe(res => {
      this.users = res;
    });
  }

  isUserSelected(user: any) {
    return this.userIds.includes(user.id);
  }

  ignoreResolvedChanged() {
  }

  onlyMyIssueChanged() {
  }

  userChanged(user: any) {
  }

  openCreateTaskModal() {
    const modal = this._modalService.create({
      nzContent: AddTaskModalComponent,
      nzClosable: false,
      nzFooter: null,
      nzWidth: 700,
      nzComponentParams: {
        boardId: this.board._id
      }
    });

    modal.afterClose.subscribe(res => {
      if (res) {
        this.issueStatuses = [];
        this.boardService.getTask().subscribe(x => {
          if (x) {
            x.map(e => {
              this.issueStatuses.push(e);
            });
          }
        });
      }
    });
  }

  modalSave(event) {
    if (event) {
      this.issueStatuses = [];
      this.boardService.getTask().subscribe(res => {
        if (res) {
          res.map(e => {
            if (e.boardId === this.board._id) {
              this.issueStatuses.push(e);
            }
          });
        }
      });
    }
  }

  openMembers() {
    this.router.navigate(['/project/members', this.board._id]);
  }

  deletteTask(task) {
    const modal = this._modalService.create({
      nzContent: IssueDeleteModalComponent,
      nzClosable: false,
      nzFooter: null,
      nzStyle: {
        top: "140px"
      },
    });

    modal.afterClose.subscribe(res => {
      if (res) {
        this.boardService.deleteTask(task).subscribe(res => {
          this.issueStatuses = [];
          this.boardService.getTask().subscribe(res => {
            if (res) {
              res.map(e => {
                if (e.boardId === this.board._id) {
                  this.issueStatuses.push(e);
                }
              });
            }
          });
        });
      }
    })
  }
}
