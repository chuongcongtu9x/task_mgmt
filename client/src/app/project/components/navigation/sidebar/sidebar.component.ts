import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { JProject } from '@trungk18/interface/project';
import { SideBarLink } from '@trungk18/interface/ui-model/nav-link';
import { SideBarLinks } from '@trungk18/project/config/sidebar';
import { ProjectQuery } from '@trungk18/project/state/project/project.query';
import { BoardService } from '@trungk18/services/board.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
@UntilDestroy()
export class SidebarComponent implements OnInit {
  @Input() expanded: boolean;
  user: any;
  boardId: any;

  get sidebarWidth(): number {
    return this.expanded ? 240 : 15;
  }

  project: JProject;
  sideBarLinks: SideBarLink[];

  constructor(private readonly boardService: BoardService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.sideBarLinks = SideBarLinks;
    this.user = JSON.parse(localStorage.getItem('currentUser'))?.user;

    this.route.params.subscribe(params => {
      this.boardService.getBoard().subscribe(res => {
        this.boardId = params['boardId'];
      })
    });
  }
}
