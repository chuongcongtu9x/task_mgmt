import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {IssueType, JIssue, IssueStatus, IssuePriority} from '@trungk18/interface/issue';
import {quillConfiguration} from '@trungk18/project/config/editor';
import {NzModalRef} from 'ng-zorro-antd/modal';
import {ProjectService} from '@trungk18/project/state/project/project.service';
import {IssueUtil} from '@trungk18/project/utils/issue';
import {ProjectQuery} from '@trungk18/project/state/project/project.query';
import {untilDestroyed, UntilDestroy} from '@ngneat/until-destroy';
import {Observable} from 'rxjs';
import {JUser} from '@trungk18/interface/user';
import {tap} from 'rxjs/operators';
import {until} from 'protractor';
import {NoWhitespaceValidator} from '@trungk18/core/validators/no-whitespace.validator';
import {DateUtil} from '@trungk18/project/utils/date';
import {BoardService} from '@trungk18/services/board.service';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'add-issue-modal',
  templateUrl: './add-issue-modal.component.html',
  styleUrls: ['./add-issue-modal.component.scss']
})
@UntilDestroy()
export class AddIssueModalComponent implements OnInit {
  reporterUsers$: Observable<JUser[]>;
  issueForm: FormGroup;
  editorOptions = quillConfiguration;
  users = [];

  @Input() task;
  boardUserId: any;
  boardId: any;

  get f() {
    return this.issueForm?.controls;
  }

  constructor(
    private _fb: FormBuilder,
    private _modalRef: NzModalRef,
    private readonly boardService: BoardService,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit(): void {
    this.initForm();
    this.boardUserId = JSON.parse(localStorage.getItem('boardUserId'));

    setTimeout(() => {
      this.boardService.getUsers().subscribe(res => {
        res.map(e => {
          if(this.boardUserId.includes(e._id)) {
            this.users.push(e);
          }
        })
      })
    }, 100);
  }

  initForm() {
    this.issueForm = this._fb.group({
      boardId: [''],
      taskId: [''],
      position: [1],
      status: [IssueType.GRAY],
      priority: [IssuePriority.MEDIUM],
      title: ['', NoWhitespaceValidator()],
      description: [''],
      reporterId: [''],
      userId: [[]]
    });
  }

  submitForm() {
    const newFormVal = _.cloneDeep(this.issueForm.getRawValue());
    newFormVal.boardId = this.task.boardId;
    newFormVal.taskId = this.task._id;
    newFormVal.status = 'gray';

    this.boardService.createTaskItem(newFormVal).subscribe(res => {
      this._modalRef.close(res);
      this.onRefresh();
    });
    this.closeModal();
  }

  onRefresh() {
    this.issueForm.reset();
  }

  cancel() {
    this.closeModal();
  }

  closeModal() {
    this._modalRef.close();
  }
}
