import { Component, OnInit } from '@angular/core';
import { quillConfiguration } from '@trungk18/project/config/editor';
import { BoardService } from '@trungk18/services/board.service';
import { NzModalRef } from 'ng-zorro-antd';

@Component({
  selector: 'app-add-task-modal',
  templateUrl: './add-board-modal.component.html',
  styleUrls: ['./add-board-modal.component.scss']
})
export class AddBoardModalComponent implements OnInit {
  editorOptions = quillConfiguration;
  taskTitle;
  taskDes;
  boardId: any;
  constructor(private _modalRef: NzModalRef, private readonly boardService: BoardService) { }

  ngOnInit() {
  }

  submit() {
    const task = {
      title: this.taskTitle,
      description: this.taskDes,
      position: 2
    };

    this.boardService.createBoard(task).subscribe(res => {
      this._modalRef.close(res);
    });
  }

  cancel() {
    this.closeModal();
  }

  closeModal() {
    this._modalRef.close();
  }

}
