import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {UntilDestroy} from '@ngneat/until-destroy';
import {JIssue} from '@trungk18/interface/issue';
import {JUser} from '@trungk18/interface/user';
import {ProjectService} from '@trungk18/project/state/project/project.service';
import {BoardService} from '@trungk18/services/board.service';

@Component({
  selector: 'issue-reporter',
  templateUrl: './issue-reporter.component.html',
  styleUrls: ['./issue-reporter.component.scss']
})
@UntilDestroy()
export class IssueReporterComponent implements OnInit, OnChanges {
  @Input() issue: any;
  @Input() users: any[];

  constructor(private readonly boardService: BoardService) {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.users) {
    }
  }

  isUserSelected(user: any) {
    return user._id === this.issue.reporterId;
  }

  updateIssue(user: any) {
    this.issue.reporterId = user._id;
    this.boardService.updateTaskIdForItem(this.issue).subscribe();
  }

  reporter() {
    return this.users?.find((x) => x._id === this.issue.reporterId);
  }
}
