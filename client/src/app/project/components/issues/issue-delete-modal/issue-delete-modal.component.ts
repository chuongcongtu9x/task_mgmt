import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NzModalRef } from 'ng-zorro-antd/modal';
import { DeleteIssueModel } from '@trungk18/interface/ui-model/delete-issue-model';

@Component({
  selector: 'issue-delete-modal',
  templateUrl: './issue-delete-modal.component.html',
  styleUrls: ['./issue-delete-modal.component.scss']
})
export class IssueDeleteModalComponent implements OnInit {
  issueId: string;

  onDelete = new EventEmitter<DeleteIssueModel>();

  constructor(private _modalRef: NzModalRef) {}

  ngOnInit(): void {}

  deleteIssue() {
    this._modalRef.close(true);
  }

  closeModal() {
    this._modalRef.close();
  }
}
