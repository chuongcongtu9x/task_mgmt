import { Component, Input, OnInit, HostListener, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { JComment } from '@trungk18/interface/comment';
import { AuthQuery } from '@trungk18/project/auth/auth.query';
import { BoardService } from '@trungk18/services/board.service';
import { CommentService } from '@trungk18/services/comment.service';

@Component({
  selector: 'issue-comment',
  templateUrl: './issue-comment.component.html',
  styleUrls: ['./issue-comment.component.scss']
})
@UntilDestroy()
export class IssueCommentComponent implements OnInit {
  @Input() issueId: string;
  @Input() comment: any;
  @Input() createMode: boolean;
  @Output() modalSave: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild('commentBoxRef') commentBoxRef: ElementRef;
  commentControl: FormControl;
  user: any;
  isEditing: boolean;
  users: any[];

  constructor(private readonly commentService: CommentService, private _authQuery: AuthQuery, private readonly boardService: BoardService) {}

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    if (!this.createMode || this.isEditing) {
      return;
    }
    if (event.key == 'M') {
      this.commentBoxRef.nativeElement.focus();
      this.isEditing = true;
    }
  }

  ngOnInit(): void {
    this.commentControl = new FormControl('');
    // this._authQuery.user$.pipe(untilDestroyed(this)).subscribe((user) => {
    //   if (this.createMode) {
    //     this.comment = new JComment(this.issueId, this.user);
    //   }
    // });
    if (this.createMode) {
      this.comment = new JComment(this.issueId, this.user);
    }

    this.user = JSON.parse(localStorage.getItem('currentUser'))?.user;

    this.boardService.getUsers().subscribe(res => {
      this.users = res;
    })
  }

  getUserComment(userId) {
    return this.users?.find(e => e._id === userId)?.fullName ?? this.user.fullName;
  }

  getAvatarComment(userId) {
    return this.users?.find(e => e._id === userId)?.avatarUrl ?? this.user.avatarUrl;
  }

  setCommentEdit(mode: boolean) {
    this.isEditing = mode;
  }

  addComment() {
    const comment = {
      body: this.commentControl.value,
      taskId: this.issueId,
      userId: this.user._id
    };

    this.commentService.createComment(comment).subscribe(res => {
      this.modalSave.emit(res);
    })
    this.cancelAddComment();
  }

  cancelAddComment() {
    this.commentControl.patchValue('');
    this.setCommentEdit(false);
  }
}
