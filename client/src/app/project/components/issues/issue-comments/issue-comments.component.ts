import { Component, Input, OnInit } from '@angular/core';
import { JIssue } from '@trungk18/interface/issue';
import { CommentService } from '@trungk18/services/comment.service';

@Component({
  selector: 'issue-comments',
  templateUrl: './issue-comments.component.html',
  styleUrls: ['./issue-comments.component.scss']
})
export class IssueCommentsComponent implements OnInit {
  @Input() issue: any;
  comments: any;

  constructor(private readonly commentService: CommentService) {}

  ngOnInit(): void {
    this.commentService.getComment().subscribe(res => {
      this.comments = res.filter(e => e.taskId === this.issue._id);
    })
  }

  modalSave(event) {
    if (event) {
      this.commentService.getComment().subscribe(res => {
        this.comments = res.filter(e => e.taskId === this.issue._id);
      })
    }
  }
}
