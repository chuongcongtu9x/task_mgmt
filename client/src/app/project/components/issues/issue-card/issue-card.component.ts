import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { JIssue } from '@trungk18/interface/issue';
import { IssuePriorityIcon } from '@trungk18/interface/issue-priority-icon';
import { JUser } from '@trungk18/interface/user';
import { ProjectQuery } from '@trungk18/project/state/project/project.query';
import { IssueUtil } from '@trungk18/project/utils/issue';
import { BoardService } from '@trungk18/services/board.service';
import { NzModalService } from 'ng-zorro-antd/modal';
import { IssueModalComponent } from '../issue-modal/issue-modal.component';

@Component({
  selector: 'issue-card',
  templateUrl: './issue-card.component.html',
  styleUrls: ['./issue-card.component.scss']
})
@UntilDestroy()
export class IssueCardComponent implements OnChanges, OnInit {
  @Input() issue: any;
  @Output() deletteTaskItem: EventEmitter<any> = new EventEmitter<any>();

  assignees: any[];
  issueTypeIcon: string;
  priorityIcon: any;
  @Input() users: any[];

  constructor(private _modalService: NzModalService, private readonly boardService: BoardService) {}

  ngOnInit(): void {
    this.assignees = this.issue.userId.map((userId) => this.users?.find((x) => x._id === userId));
  }

  ngOnChanges(changes: SimpleChanges): void {
    const issueChange = changes.issue;
    if (issueChange?.currentValue !== issueChange.previousValue) {
      this.issueTypeIcon = IssueUtil.getIssueTypeIcon(this.issue.type);
      this.priorityIcon = IssueUtil.getIssuePriorityIcon(this.issue.priority);
    }
  }

  openIssueModal(issue: string) {
    const modal = this._modalService.create({
      nzContent: IssueModalComponent,
      nzWidth: 1040,
      nzClosable: false,
      nzFooter: null,
      nzComponentParams: {
        issue
      }
    });

    modal.afterClose.subscribe(res => {
      if (res) {
        this.deletteTaskItem.emit(issue);
      }
    })
  }
}
