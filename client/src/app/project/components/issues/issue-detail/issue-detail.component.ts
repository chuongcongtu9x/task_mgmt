import { Component, OnInit, Input, Output, EventEmitter, OnChanges, TemplateRef, ViewChild } from '@angular/core';
import { JIssue } from '@trungk18/interface/issue';
import { ProjectQuery } from '@trungk18/project/state/project/project.query';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { IssueDeleteModalComponent } from '../issue-delete-modal/issue-delete-modal.component';
import { DeleteIssueModel } from '@trungk18/interface/ui-model/delete-issue-model';
import { BoardService } from '@trungk18/services/board.service';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'issue-detail',
  templateUrl: './issue-detail.component.html',
  styleUrls: ['./issue-detail.component.scss']
})
export class IssueDetailComponent implements OnInit, OnChanges {
  @ViewChild('addWorkNeedModal', { static: false }) addWorkNeedModal?: TemplateRef<any>;
  @Input() issue: any;
  @Input() isShowFullScreenButton: boolean;
  @Input() isShowCloseButton: boolean;
  @Output() onClosed = new EventEmitter();
  @Output() onOpenIssue = new EventEmitter<string>();
  @Output() onDelete = new EventEmitter<DeleteIssueModel>();
  timeLimit: any;

  users = [];
  isDisplayAddWorkNeed = false;
  form: FormGroup;
  titleWordNeed = '';
  titleWorkNeedParent = '';
  modal;
  showTimeLimit = false;
  boardUserId: any;
  boardId: any;

  constructor(public projectQuery: ProjectQuery, private _modalService: NzModalService, private readonly boardService: BoardService,
    private fb: FormBuilder,
    private nzModalService: NzModalService,
    private _modalRef: NzModalRef,
    private route: ActivatedRoute
  ) {
    this.initForm();
  }

  ngOnInit(): void {
    console.log('issue', this.issue);
    this.boardUserId = JSON.parse(localStorage.getItem('boardUserId'));

    this.boardService.getUsers().subscribe(res => {
      res.map(e => {
        if (this.boardUserId.includes(e._id)) {
          this.users.push(e);
        }
      })
    })

    if (this.issue?.timeLimit) {
      this.timeLimit = this.issue.timeLimit;
    }
  }

  ngOnChanges() {
    if (this.issue?.workNeedTitle) {
      this.titleWorkNeedParent = this.issue.workNeedTitle;
    }
    if (this.issue && this.issue.fromValue) {
      const fromValue = JSON.parse(this.issue.fromValue);

      this.form.patchValue(fromValue);
      // competent Persons
      if (fromValue?.listOfData?.length > 0) {
        const documents = _.compact(_.cloneDeep(fromValue?.listOfData));
        this.getListOfData.clear();
        documents.forEach(item => {
          const formGroup = this.initListOfData();
          this.getListOfData.push(formGroup);

          // Patch value from store.
          formGroup.patchValue(item);
        });
      }
    }
  }

  get getListOfData() {
    return this.form.get('listOfData') as FormArray;
  }

  openDeleteIssueModal() {
    const modal = this._modalService.create({
      nzContent: IssueDeleteModalComponent,
      nzClosable: false,
      nzFooter: null,
      nzStyle: {
        top: "140px"
      }
    });

    modal.afterClose.subscribe(res => {
      if (res) {
        this.boardService.deleteTaskItem(this.issue).subscribe();
        this._modalRef.close(this.issue);
      }
    })
  }

  closeModal() {
    this.onClosed.emit();
  }

  openIssuePage() {
    this.onOpenIssue.emit(this.issue.id);
  }

  initForm() {
    this.form = this.fb.group({
      listOfData: new FormArray([])
    });
  }

  saveFromValue(valueForm) {
    if (valueForm) {
      const fromValue = JSON.stringify(valueForm);
      this.issue.fromValue = fromValue;
      this.boardService.updateTaskIdForItem(this.issue).subscribe(res => {
        const issueStatuses = [];
        this.boardService.getTask().subscribe(res => {
          if (res) {
            this.issue = res.find(e => e.boardId === this.issue.boardId && e.taskId === this.issue.taskId).taskItem.find(e => e._id === this.issue._id);
            const fromValue = JSON.parse(this.issue.fromValue);

            this.form.patchValue(fromValue);
            if (fromValue?.listOfData?.length > 0) {
              const documents = _.compact(_.cloneDeep(fromValue?.listOfData));
              this.getListOfData.clear();
              documents.forEach(item => {
                const formGroup = this.initListOfData();
                this.getListOfData.push(formGroup);
                formGroup.patchValue(item);
              });
            }
          }
        })
      })
    }
  }

  onAdd(value) {
    if (this.titleWordNeed === '') return;
    this.getListOfData.push(this.initListOfData(value) as FormGroup);
    this.isDisplayAddWorkNeed = false;
    this.titleWordNeed = '';
    const valueForm = _.cloneDeep(this.form.getRawValue());
    this.saveFromValue(valueForm);
  }

  onClickNeed(i) {
    const valueForm = _.cloneDeep(this.form.getRawValue());
    valueForm.listOfData = valueForm.listOfData.map((e, index) => {
      if (index === i) {
        e.checked = !e.checked;
        return e;
      }
      return e;
    });

    this.saveFromValue(valueForm);
  }

  change(value, i) {
    const valueForm = _.cloneDeep(this.form.getRawValue());
    valueForm.listOfData = valueForm.listOfData.map((e, index) => {
      if (index === i) {
        e.title = value.target.value;
        return e;
      }
      return e;
    });

    this.saveFromValue(valueForm);
  }

  private initListOfData(value?) {
    if (value) {
      return this.fb.group({
        title: [value, Validators.compose([Validators.maxLength(255), Validators.required])],
        checked: false
      });
    }

    return this.fb.group({
      title: [''],
      checked: false
    });
  }

  getPrecent() {
    const { value } = this.getListOfData;

    const precent = Math.round((value.filter(e => e.checked).length / value.length) * 100);
    return precent ? precent : 0;
  }

  addTitleWorkNeed() {
    this.modal = this._modalService.create({
      nzContent: this.addWorkNeedModal,
      nzClosable: false,
      nzFooter: null,
      nzWidth: 500,
    });
  }

  submitWorkNeedTitle() {
    this.issue.workNeedTitle = this.titleWorkNeedParent;
    this.boardService.updateTaskIdForItem(this.issue).subscribe(res => {
      this.modal.close();
    });
  }

  removeWorkNeed() {
    this.issue.workNeedTitle = '';
    this.titleWorkNeedParent = '';
    this.boardService.updateTaskIdForItem(this.issue).subscribe(res => {
    });
  }

  deleteWorkNeedItem(item) {
    console.log(item);
    // this.images.removeAt(this.images.value.findIndex(image => image.id === 502))
  }

  cancel() {
    this.modal.close();
  }

  onSave() {
    this.issue.timeLimit = this.timeLimit;
    this.boardService.updateTaskIdForItem(this.issue).subscribe(res => {
      this.showTimeLimit = true;
    })
  }
}
