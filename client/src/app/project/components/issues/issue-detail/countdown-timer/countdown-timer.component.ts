import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'countdown-timer',
  template: `<strong style="color: red;">{{countDowntime}}</strong>`
})
export class CountdownTimerComponent implements OnInit, OnChanges {
  @Input() toDate: any;
  countDowntime: string;
  counter;
  private launchDate: Date;
  ngOnInit() {
    this.displayTime(new Date(this.toDate), 'vi');
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.toDate) {
      console.log(new Date(this.toDate));
    }
  }

  private displayTime(date: any, lang: string) {
    if (date) {
      clearInterval(this.counter);
      this.launchDate = new Date(date);

      this.counter = setInterval(() => {
        const difference = (this.launchDate.getTime() - new Date().getTime());
        this.countDowntime = this.calculate(this.launchDate, lang);
        // this.countDowntime = this.calculate(difference, lang);
        if (difference < 0) {
          console.log('lang', lang);
          this.countDowntime = ' ' + String('vi').toUpperCase() === String(lang).toUpperCase() ? 'Expired' : 'Hết hạn';
          clearInterval(this.counter);
        }
      }, 1000);

    } else {
      this.countDowntime = 'N/a';
    }
  }

  daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
  }

  private calculate(date: any, lang: string) {
    const distance = this.launchDate.getTime() - new Date().getTime();
    const days = Math.floor(distance / (1000 * 60 * 60 * 24));
    const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((distance % (1000 * 60)) / 1000);

    let datetimeMsg = '';

    const secondLang = String('vi').toUpperCase();
    const dayText = secondLang === String(lang).toUpperCase() ? 'days' : 'ngày';
    const hourText = secondLang === String(lang).toUpperCase() ? 'hours' : 'giờ';
    const minuteText = secondLang === String(lang).toUpperCase() ? 'minutes' : 'phút';
    const secondText = secondLang === String(lang).toUpperCase() ? 'seconds' : 'giây';

    if (days > 0) {
      datetimeMsg += ` ${this.numberIsLessThan10(days)} ${dayText}`;
    }
    if (hours >= 0) {
      datetimeMsg += ` ${this.numberIsLessThan10(hours)} ${hourText}`;
    }
    if (minutes >= 0) {
      datetimeMsg += ` ${this.numberIsLessThan10(minutes)} ${minuteText}`;
    }
    if (seconds >= 0) {
      datetimeMsg += ` ${this.numberIsLessThan10(seconds)} ${secondText}`;
    }
    return datetimeMsg;
  }

  private numberIsLessThan10(num: number): string {
    if (num >= 0 && num < 10) {
      return '0' + String(num);
    }

    return String(num);
  }
}
