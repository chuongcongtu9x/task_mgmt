import { Component, Input, OnInit } from '@angular/core';
import { IssueStatus, IssueStatusDisplay, JIssue } from '@trungk18/interface/issue';
import { ProjectService } from '@trungk18/project/state/project/project.service';
import { ProjectQuery } from '@trungk18/project/state/project/project.query';
import { BoardService } from '@trungk18/services/board.service';

@Component({
  selector: 'issue-status',
  templateUrl: './issue-status.component.html',
  styleUrls: ['./issue-status.component.scss']
})
export class IssueStatusComponent implements OnInit {
  @Input() issue: JIssue;

  issueStatuses: any[];

  constructor(private readonly boardService: BoardService) {}

  ngOnInit(): void {
    this.issueStatuses = [
      {
        value: 'red',
        color: 'red'
      },
      {
        value: 'green',
        color: 'green'
      },
      {
        value: 'yellow',
        color: 'yellow'
      },
      {
        value: 'gray',
        color: 'gray'
      },
    ];
  }

  updateIssue(status: IssueStatus) {
    this.issue.status = status;
    this.boardService.updateTaskIdForItem(this.issue).subscribe(res => {
    });
  }

  isStatusSelected(status: IssueStatus) {
    return this.issue.status === status;
  }
}

class IssueStatusValueTitle {
  value: IssueStatus;
  label: string;
  constructor(issueStatus: IssueStatus) {
    this.value = issueStatus;
    this.label = IssueStatusDisplay[issueStatus];
  }
}
