import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { UntilDestroy } from '@ngneat/until-destroy';
import { BoardService } from '@trungk18/services/board.service';

@Component({
  selector: 'issue-assignees',
  templateUrl: './issue-assignees.component.html',
  styleUrls: ['./issue-assignees.component.scss']
})
@UntilDestroy()
export class IssueAssigneesComponent implements OnInit, OnChanges {
  @Input() issue: any;
  @Input() users: any[];
  assignees: any[];

  constructor(private readonly boardService: BoardService) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.assignees = this.issue.userId.map((userId) => this.users?.find((x) => x._id === userId));
    }, 200);
  }

  ngOnChanges(changes: SimpleChanges) {
    let issueChange = changes.issue;
    if (this.users && issueChange?.currentValue !== issueChange?.previousValue) {
      this.assignees = this.issue.userId.map((userId) => this.users.find((x) => x._id === userId));
    }
  }

  removeUser(userId: string) {
    this.assignees = this.assignees.filter(e => e._id !== userId);
    this.issue.userId = this.assignees.map(e => e._id);
    this.boardService.updateTaskIdForItem(this.issue).subscribe();
  }

  addUserToIssue(user: any) {
    this.assignees.push(user);
    this.issue.userId = this.assignees.map(e => e._id);
    this.boardService.updateTaskIdForItem(this.issue).subscribe();
  }

  isUserSelected(user: any): boolean {
    return this.issue.userId.includes(user._id);
  }
}
