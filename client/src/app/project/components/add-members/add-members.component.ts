import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { BoardService } from '@trungk18/services/board.service';
import { NzModalRef } from 'ng-zorro-antd';
import * as _ from 'lodash';

@Component({
  selector: 'app-add-members',
  templateUrl: './add-members.component.html',
  styleUrls: ['./add-members.component.scss']
})
export class AddMembersComponent implements OnInit {
  users = [];
  issueForm: FormGroup;

  @Input() boardUserId;
  @Input() boardId;

  get f() {
    return this.issueForm?.controls;
  }

  constructor(
    private _fb: FormBuilder,
    private _modalRef: NzModalRef,
    private readonly boardService: BoardService,
  ) {}

  ngOnInit(): void {
    this.initForm();

    setTimeout(() => {
      this.boardService.getUsers().subscribe(res => {
        res.map(e => {
          if(!this.boardUserId.includes(e._id)) {
            this.users.push(e);
          }
        })
      })
    }, 100);
  }

  initForm() {
    this.issueForm = this._fb.group({
      userId: [[]]
    });
  }

  submitForm() {
    const newFormVal = _.cloneDeep(this.issueForm.getRawValue());
    newFormVal._id = this.boardId;
    newFormVal.userId = newFormVal.userId.concat(this.boardUserId);
    this.boardService.updateBoard(newFormVal).subscribe(res => {});
    this._modalRef.close(true);
  }

  onRefresh() {
    this.issueForm.reset();
  }

  cancel() {
    this.closeModal();
  }

  closeModal() {
    this._modalRef.close();
  }
}
