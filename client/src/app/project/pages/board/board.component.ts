import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {BoardService} from '@trungk18/services/board.service';

@Component({
  selector: 'board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {
  breadcrumbs: string[] = ['Projects', 'Angular Jira Clone', 'Kanban Board'];
  board: any;
  boardUserId: any;

  constructor(private router: Router, private readonly boardService: BoardService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    localStorage.removeItem('boardUserId');
    this.route.params.subscribe(params => {
      this.boardService.getBoard().subscribe(res => {
        this.board = res.find(e => e._id === params.boardId);
        this.boardUserId = res.find(e => e._id === params?.boardId)?.userId;
        localStorage.setItem('boardUserId', JSON.stringify(this.boardUserId));
      });
    });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }
}
