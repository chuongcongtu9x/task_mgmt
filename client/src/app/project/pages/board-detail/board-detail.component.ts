import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AddBoardModalComponent } from '@trungk18/project/components/add-board-modal/add-board-modal.component';
import { BoardService } from '@trungk18/services/board.service';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'app-board-detail',
  templateUrl: './board-detail.component.html',
  styleUrls: ['./board-detail.component.scss']
})
export class BoardDetailComponent implements OnInit {
  boards: any[];
  constructor(private readonly boardService: BoardService, private _modalService: NzModalService, private router: Router) { }

  ngOnInit() {
    this.boardService.getBoard().subscribe(res => {
      this.boards = res;
    })
  }

  openCreateBoardModal() {
    const modal = this._modalService.create({
      nzContent: AddBoardModalComponent,
      nzClosable: false,
      nzFooter: null,
      nzWidth: 700
    });

    modal.afterClose.subscribe(res => {
      this.boardService.getBoard().subscribe(res => {
        this.boards = res;
      })
    })
  }

  openBoardDetail(item) {
    this.router.navigate(['/project/board', item._id])
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }
}
