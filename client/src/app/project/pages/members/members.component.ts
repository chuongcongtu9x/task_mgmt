import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AddBoardModalComponent } from '@trungk18/project/components/add-board-modal/add-board-modal.component';
import { AddMembersComponent } from '@trungk18/project/components/add-members/add-members.component';
import { BoardService } from '@trungk18/services/board.service';
import { NzModalService } from 'ng-zorro-antd';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit {
  users = [];
  user: any;
  boardUserId: any;
  boardId: any;

  constructor(private readonly boardService: BoardService, private _modalService: NzModalService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.boardService.getBoard().subscribe(res => {
        this.boardUserId = res.find(e => e._id === params['boardId']).userId;
        this.boardId = params['boardId'];
      })
    });

    this.user = JSON.parse(localStorage.getItem('currentUser'))?.user;
    setTimeout(() => {
      this.boardService.getUsers().subscribe(res => {
        res.map(e => {
          if (this.boardUserId.includes(e._id)) {
            this.users.push(e);
          }
        })
      })
    }, 100);
  }

  outMenber(id) {
    var obj = {
      userId: this.boardUserId.filter(e => e !== id),
      _id: this.boardId
    }
    this.boardService.updateBoard(obj).subscribe(res => {
      this.users = [];
      this.boardService.getBoard().subscribe(res => {
        this.boardUserId = res.find(e => e._id === this.boardId).userId;
        this.boardService.getUsers().subscribe(res => {
          res.map(e => {
            if (this.boardUserId.includes(e._id)) {
              this.users.push(e);
            }
          })
        })
      });
    });
  }

  logout() {
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
  }

  openCreateMemberModal() {
    const modal = this._modalService.create({
      nzContent: AddMembersComponent,
      nzClosable: false,
      nzFooter: null,
      nzWidth: 700,
      nzComponentParams: {
        boardUserId: this.boardUserId,
        boardId: this.boardId
      }
    });

    modal.afterClose.subscribe(res => {
      if (res) {
        this.users = [];
        this.boardService.getUsers().subscribe(res => {
          res.map(e => {
            if (this.boardUserId.includes(e._id)) {
              this.users.push(e);
            }
          })
        })
      }
    })
  }
}
