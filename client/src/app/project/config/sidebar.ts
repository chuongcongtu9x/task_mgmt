import { SideBarLink } from '@trungk18/interface/ui-model/nav-link';

export const SideBarLinks = [
  new SideBarLink('Board', 'board', 'board-detail'),
  new SideBarLink('Project Settings', 'cog', 'settings'),
];
