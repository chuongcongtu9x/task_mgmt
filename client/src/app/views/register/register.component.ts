import {ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {LoginInput} from '@trungk18/models/LoginInput';
import {LoginService} from '@trungk18/services/login.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  isLoggedin: boolean;
  public form: FormGroup;

  constructor(
    private readonly router: Router,
    private readonly loginService: LoginService,
    public fb: FormBuilder, private cd: ChangeDetectorRef
  ) {
    this.createForm();
  }

  ngOnInit() {
  }

  submit() {
    console.log(this.registrationForm)
    const newFormVal = _.cloneDeep(this.form.getRawValue());
    // newFormVal.avatarUrl = this.registrationForm.value.file;
    newFormVal.avatarUrl = 'https://i.pinimg.com/236x/d6/27/d9/d627d9cda385317de4812a4f7bd922e9--man--iron-man.jpg';

    this.loginService.register(newFormVal).subscribe(res => {
      if (res) {
        this.form.reset();
        this.router.navigate(['/login']);
      }
    });
  }

  private createForm() {
    this.form = this.fb.group({
      username: [{value: null, disabled: false}, Validators.compose([Validators.required])],
      fullName: [{value: null, disabled: false}],
      password: [{value: null, disabled: false}, Validators.compose([Validators.required])],
      gmail: [{value: null, disabled: false}],
      // avatarUrl: [{value: null, disabled: false}],
    });
  }

  registrationForm = this.fb.group({
    file: [null],
  });

  /*########################## File Upload ########################*/
  @ViewChild('fileInput') el: ElementRef;
  imageUrl: any =
    'https://i.pinimg.com/236x/d6/27/d9/d627d9cda385317de4812a4f7bd922e9--man--iron-man.jpg';
  editFile: boolean = true;
  removeUpload: boolean = false;

  uploadFile(event) {
    let reader = new FileReader(); // HTML5 FileReader API
    let file = event.target.files[0];
    if (event.target.files && event.target.files[0]) {
      reader.readAsDataURL(file);

      // When file uploads set it to file formcontrol
      reader.onload = () => {
        this.imageUrl = reader.result;
        this.registrationForm.patchValue({
          file: reader.result,
        });
        this.editFile = false;
        this.removeUpload = true;
      };
      // ChangeDetectorRef since file is loading outside the zone
      this.cd.markForCheck();
    }
  }

  // Function to remove uploaded file
  removeUploadedFile() {
    let newFileList = Array.from(this.el.nativeElement.files);
    this.editFile = true;
    this.removeUpload = false;
    this.registrationForm.patchValue({
      file: [null],
    });
  }

  // Submit Registration Form
  onSubmit() {
    if (!this.registrationForm.valid) {
      alert('Please fill all the required fields to create a super hero!');
      return false;
    } else {
      console.log(this.registrationForm.value);
    }
  }
}
