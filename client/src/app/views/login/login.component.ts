import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '@trungk18/services/login.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import * as _ from 'lodash';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {
  isLoggedin: boolean;
  public form: FormGroup;

  constructor(
    private readonly router: Router,
    private readonly loginService: LoginService,
    private fb: FormBuilder
  ) {
    this.createForm();
  }
  ngOnInit() {
    const user = localStorage.getItem('currentUser');
    if (user) {
      this.router.navigate(['/project']);
    }
  }

  submit() {
    const newFormVal = _.cloneDeep(this.form.getRawValue());
    this.loginService.login(newFormVal).subscribe(res => {
      if (res) {
        this.router.navigate(['/project']);
      }
    });
  }

  loginWithGoogle(): void {
  }

  logOut(): void {
  }

  private createForm() {
    this.form = this.fb.group({
      username: [{value: null, disabled: false}, Validators.compose([Validators.required])],
      password: [{value: null, disabled: false}, Validators.compose([Validators.required])],
    });
  }
}
