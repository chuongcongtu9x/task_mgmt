import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class BoardService {
    readonly APIUrl = 'http://localhost:5100/api';

    constructor(private http: HttpClient) { }

    httpOptxions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getBoard(): Observable<any[]> {
        return this.http.get<any>(
            `${this.APIUrl}/board`
        );
    }

    createBoard(board: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/board`, board
        );
    }
    updateBoard(board: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/board/update`, board
        );
    }

    getTask(): Observable<any[]> {
        return this.http.get<any>(
            `${this.APIUrl}/taskItem`
        );
    }

    createTask(task: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/task`, task
        );
    }

    createTaskItem(taskItem: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/taskItem`, taskItem
        );
    }

    updateTaskIdForItem(preTaskItem: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/taskItem/updateTaskId`, preTaskItem
        );
    }

    deleteTask(task: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/task/delete`, task
        );
    }

    getUsers(): Observable<any[]> {
        return this.http.get<any>(
            `${this.APIUrl}/board/users`
        );
    }

    deleteTaskItem(id: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/taskItem/delete`, id
        );
    }
}
