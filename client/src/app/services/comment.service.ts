import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class CommentService {
    readonly APIUrl = 'http://localhost:5100/api';

    constructor(private http: HttpClient) { }

    httpOptxions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    };

    getComment(): Observable<any[]> {
        return this.http.get<any>(
            `${this.APIUrl}/comment`
        );
    }

    createComment(comment: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/comment`, comment
        );
    }

    updateComment(preTaskItem: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/comment/update`, preTaskItem
        );
    }

    deleteComment(comment: any): Observable<any[]> {
        return this.http.post<any>(
            `${this.APIUrl}/comment/delete`, comment
        );
    }
}
