import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  readonly APIUrl = 'http://localhost:5100';

  constructor(private http: HttpClient) { }

  httpOptxions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  login(val: any): Observable<any> {
      return this.http.post<any>(this.APIUrl + '/api/auth/login', val)
      .pipe(map(user => {
        if (user && user.accessToken) {
          localStorage.setItem('currentUser', JSON.stringify(user));
      }

        return user;
      }))
  }

  register(val: any): Observable<any> {
    return this.http.post<any>(this.APIUrl + '/api/auth/register', val);
}
}
