import express from 'express';
import cors from 'cors';
import board from './routers/board.js';
import task from './routers/task.js';
import taskItem from './routers/taskItem.js';
import comment from './routers/comment.js';
import mongoose from 'mongoose';
import authRouter from './routers/auth.js'

// import dotenv from 'dotenv';

// dotenv.config();

const app = express();
const PORT = process.env.PORT || 5100;

const URI = 'mongodb+srv://fis:TranChuong22662@cluster0.zxbxx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

app.use(cors());
app.use(express.json({ limit: '30mb' }));
app.use(express.urlencoded({ extended: true, limit: '30mb' }));

app.use('/api/auth', authRouter);
app.use('/api/board', board);
app.use('/api/task', task);
app.use('/api/taskItem', taskItem);
app.use('/api/comment', comment)


mongoose
  .connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    console.log('Connected to DB');
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((err) => {
    console.log('err', err);
  });
