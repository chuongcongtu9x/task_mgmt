import express from 'express';
import { createBoard, getBoards, getTasks, getUsers, updateBoard } from '../controllers/board.js';
import verifyToken from '../middlewares/auth.js';

const router = express.Router();

router.get('/', getBoards);

router.get('/task', getTasks);

router.post('/', createBoard);

router.post('/update', updateBoard);

router.get('/users', getUsers);


export default router;
