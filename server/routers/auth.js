import express from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import verifyToken from '../middlewares/auth.js';
import { UserModel } from '../models/UserModel.js';
const router = express.Router()

// @route GET api/auth
// @desc Check if user is logged in
// @access Public
router.get('/', verifyToken, async (req, res) => {
	try {
		const user = await UserModel.findById(req.userId).select('-password')
		if (!user)
			return res.status(400).json({ success: false, message: 'User not found' })
		res.json({ success: true, user })
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
})

// // @route POST api/auth/register
// // @desc Register user
// // @access Public
router.post('/register', async (req, res) => {
	const { username, password, gmail, fullName, avatarUrl } = req.body

	// Simple validation
	if (!username || !password)
		return res
			.status(400)
			.json({ success: false, message: 'Missing username and/or password' })

	try {
		// Check for existing user
		const user = await UserModel.findOne({ username })

		if (user)
			return res
				.status(400)
				.json({ success: false, message: 'Username already taken' })

		// All good
		const hashedPassword = await argon2.hash(password)
		const newUser = new UserModel({ username, password: hashedPassword, fullName, avatarUrl, gmail })
		await newUser.save()

		// Return token
		const accessToken = jwt.sign(
			{ userId: newUser._id },
			'yoursecret'
		)

		res.json({
			success: true,
			message: 'User created successfully',
			accessToken
		})
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
})

router.post('/registerGoogle', async (req, res) => {
	const { username, password } = req.body

	try {
		// Check for existing user
		const user = await UserModel.findOne({ username })

		if (user) {
			const passwordValid = await argon2.verify(user.password, password)
			if (!passwordValid)
				return res
					.status(400)
					.json({ success: false, message: 'Incorrect username or password' })

			// All good
			// Return token
			const accessToken = jwt.sign(
				{ userId: user._id },
				'yoursecret'
			)

			res.json({
				success: true,
				message: 'User logged in successfully',
				accessToken
			})
		} else {
			// All good
			const hashedPassword = await argon2.hash(password)
			const newUser = new UserModel({ username, password: hashedPassword })
			await newUser.save()

			// Return token
			const accessToken = jwt.sign(
				{ userId: newUser._id },
				'yoursecret'
			)

			res.json({
				success: true,
				message: 'User created successfully',
				accessToken
			})
		}
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
})

// @route POST api/auth/login
// @desc Login user
// @access Public
router.post('/login', async (req, res) => {
	const { username, password } = req.body

	// Simple validation
	if (!username || !password)
		return res
			.status(400)
			.json({ success: false, message: 'Missing username and/or password' })

	try {
		// Check for existing user
		const user = await UserModel.findOne({ username })
		if (!user)
			return res
				.status(400)
				.json({ success: false, message: 'Incorrect username or password' })

		// Username found
		const passwordValid = await argon2.verify(user.password, password)
		if (!passwordValid)
			return res
				.status(400)
				.json({ success: false, message: 'Incorrect username or password' })

		// All good
		// Return token
		const accessToken = jwt.sign(
			{ userId: user._id },
			'yoursecret'
		)

		res.json({
			success: true,
			message: 'User logged in successfully',
			accessToken,
			user: user
		})
	} catch (error) {
		console.log(error)
		res.status(500).json({ success: false, message: 'Internal server error' })
	}
})

export default router;
