import express from 'express';
import { createComment, deleteComment, getComments, updateComment } from '../controllers/comment.js';

const router = express.Router();

router.get('/', getComments);

router.post('/', createComment);

router.post('/update', updateComment);

router.post('/delete', deleteComment);

export default router;
