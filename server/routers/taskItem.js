import express from 'express';
import { createTaskItem, getTaskItem, updateTaskItem, updateTaskIdForItem, deleteTaskItem } from '../controllers/taskItem.js';

const router = express.Router();

router.get('/', getTaskItem);

router.post('/', createTaskItem);

router.post('/update', updateTaskItem);

router.post('/updateTaskId', updateTaskIdForItem);

router.post('/delete', deleteTaskItem);

export default router;
