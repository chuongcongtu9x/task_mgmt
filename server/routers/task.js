import express from 'express';
import { createTask, deleteTask, getTasks, updateTask } from '../controllers/task.js';

const router = express.Router();

router.get('/', getTasks);

router.post('/', createTask);

router.post('/update', updateTask);

router.post('/delete', deleteTask);

export default router;
