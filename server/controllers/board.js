import { BoardModel } from '../models/BoardModel.js';
import { TaskModel } from '../models/TaskModel.js';
import { UserModel } from '../models/UserModel.js';

export const getBoards = async (req, res) => {
  try {
    const boards = await BoardModel.find();

    res.status(200).json(boards);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const getUsers = async (req, res) => {
  try {
    const users = await UserModel.find();

    res.status(200).json(users);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const getTasks = async (req, res) => {
    try {
      const tasks = await TaskModel.aggregate([
          {
              $lookup:
                  {
                      from: "taskitems",
                      localField: "id",
                      foreignField: "id",
                      as: "taskItem"
                  }
          }
      ]);
  
      res.status(200).json(tasks);
    } catch (err) {
      res.status(500).json({ error: err });
    }
  };

export const createBoard = async (req, res) => {
  try {
    const newBoard = req.body;

    const board = new BoardModel(newBoard);
    await board.save();

    res.status(200).json(board);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateBoard = async (req, res) => {
  try {
    const updateBoard = req.body;

    const board = await BoardModel.findOneAndUpdate(
      { _id: updateBoard._id },
      updateBoard,
      { new: true }
    );

    res.status(200).json(board);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

