import { TaskModel } from '../models/TaskModel.js';

export const getTasks = async (req, res) => {
  try {
    const tasks = await TaskModel.find();

    res.status(200).json(tasks);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const createTask = async (req, res) => {
  try {
    const newTask = req.body;

    const task = new TaskModel(newTask);
    await task.save();

    res.status(200).json(task);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateTask = async (req, res) => {
  try {
    const updateTask = req.body;

    const task = await TaskModel.findOneAndUpdate(
      { _id: updateTask._id },
      updateTask,
      { new: true }
    );

    res.status(200).json(task);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const deleteTask = async (req, res) => {
  try {
    const deleteTask = req.body;

    const task = await TaskModel.findByIdAndDelete(
      { _id: deleteTask._id },
      deleteTask
    );

    res.status(200).json(deleteTask);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

