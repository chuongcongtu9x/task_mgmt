import { TaskItemModel } from '../models/TaskItemModel.js';
import { TaskModel } from '../models/TaskModel.js';

export const getTaskItem = async (req, res) => {
  try {
    const taskItem = await TaskModel.aggregate([
      { "$addFields": { "taskId": { "$toString": "$_id" } } },
      {
        $lookup:
        {
          from: "taskitems",
          localField: "taskId",
          foreignField: "taskId",
          as: "taskItem"
        }
      }
    ]);

    res.status(200).json(taskItem);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const createTaskItem = async (req, res) => {
  try {
    const newTaskItem = req.body;

    const taskItem = new TaskItemModel(newTaskItem);
    await taskItem.save();

    res.status(200).json(taskItem);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateTaskItem = async (req, res) => {
  try {
    const updateTaskItem = req.body;

    const taskItem = await TaskItemModel.findOneAndUpdate(
      { _id: updateTaskItem._id },
      updateTaskItem,
      { new: true }
    );

    res.status(200).json(taskItem);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateTaskIdForItem = async (req, res) => {
  try {
    const updateTaskItem = req.body;

    const taskItem = await TaskItemModel.findOneAndUpdate(
      { _id: updateTaskItem._id },
      updateTaskItem,
      { new: true }
    );
      
    res.status(200).json(taskItem);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const deleteTaskItem = async (req, res) => {
  try {
    const deleteTask = req.body;

    const task = await TaskItemModel.findByIdAndDelete(
      { _id: deleteTask._id },
      deleteTask
    );

    res.status(200).json(deleteTask);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};



