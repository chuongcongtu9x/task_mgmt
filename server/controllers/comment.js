import { CommentModel } from '../models/CommentModel.js';

export const getComments = async (req, res) => {
  try {
    const comments = await CommentModel.find();

    res.status(200).json(comments);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const createComment = async (req, res) => {
  try {
    const newComment = req.body;

    const comment = new CommentModel(newComment);
    await comment.save();

    res.status(200).json(comment);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const updateComment = async (req, res) => {
  try {
    const commentTask = req.body;

    const comment = await CommentModel.findOneAndUpdate(
      { _id: commentTask._id },
      commentTask,
      { new: true }
    );

    res.status(200).json(comment);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

export const deleteComment = async (req, res) => {
  try {
    const commentTask = req.body;

    const task = await TaskModel.findByIdAndDelete(
      { _id: commentTask._id },
      commentTask
    );

    res.status(200).json(commentTask);
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

