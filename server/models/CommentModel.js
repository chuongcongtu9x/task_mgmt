import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    body: {
      type: String,
      required: true,
    },
    taskId: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
  },
  { timestamps: true }
);

export const CommentModel = mongoose.model('Comment', schema);