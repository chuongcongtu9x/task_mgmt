import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    userId: {
      type: Array,
    },
    isDelete: {
      type: Boolean
    },
    userAdminId: {
      type: Array
    }
  },
  { timestamps: true }
);

export const BoardModel = mongoose.model('Board', schema);
