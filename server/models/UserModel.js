import mongoose from 'mongoose';

const schema = new mongoose.Schema({
	username: {
		type: String,
		required: true,
		unique: true
	},
	fullName: {
		type: String,
	},
	password: {
		type: String,
		required: true
	},
	gmail: {
		type: String,
	},
	avatarUrl: {
		type: String,
	},
	boardId: {
		type: Array,
	},
},
	{ timestamps: true }
);

export const UserModel = mongoose.model('Users', schema);

