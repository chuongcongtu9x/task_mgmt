import mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
    },
    boardId: {
        type: String,
        required: true,
    },
    position: {
        type: Number,
        required: true,
    },
  },
  { timestamps: true }
);

export const TaskModel = mongoose.model('Task', schema);
