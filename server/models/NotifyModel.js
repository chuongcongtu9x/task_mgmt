import mongoose from 'mongoose';

const schema = new mongoose.Schema(
    {
        title: {
            type: String,
        },
        boardId: {
            type: String,
        },
        userId: {
            type: String,
        },
        countMsg: {
            type: Number,
        },
    },
    { timestamps: true }
);

export const CommentModel = mongoose.model('Comment', schema);
